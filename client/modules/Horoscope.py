# -*- coding: utf-8-*-
import re
import json
import urllib2
from urllib import urlopen
from client.mic import Mic

WORDS = ["HOROSCOPE"]
		
def handle(text, mic, profile):
    # Vocab used in Currency Exchange Rate Module
    phrases = ["ARIES", "TAURUS", "GEMINI", "CANCER","LEO", "VIRGO",
        "LIBRA", "SCORPIO", "SAGITTARIUS", "CAPRICORN", "AQUARIUS", "PISCES"]

    # Comment out below two lines if you are using Jasper in TEXT Mode			
    currexch_stt_engine = mic.active_stt_engine.get_instance('rateexch', phrases)
    mic = Mic(mic.speaker, mic.passive_stt_engine, currexch_stt_enginess)
		
    def getHoroscope(sign, date):
        yql_query_url = "https://api.moonbase.space/getHoroscope?sign="+sign+"&date="+date

        yql_response = urllib2.urlopen(yql_query_url)
        yql_json = json.loads(yql_response.read())
        text_output = yql_json['text']
        return text_output

    while True:
            mic.say(" What sign?")
            sign = mic.activeListen()

            mic.say(" What time?")
            date = mic.activeListen()

            if date == "":
                date = 'today'
            
            if sign != "":
                mic.say(" Getting horoscope for " + sign + " for " + date + ".")

                try:
                    # MOONBASE Services 
                    horosc = getHoroscope(sign, date)
                     
                except (IOError, ValueError, KeyError, TypeError):
                    pass # invalid json
                    mic.say(" An error occurred. Maybe the A P I is off line?")
                    break 

                else:
                    pass # valid json
                    mic.say(" Okay, here is your horoscope.")
                    mic.say(horosc)
                    mic.say(" Do you want to check another sign?")
                    ans = mic.activeListen()
                    if "NO" in ans.split():
                        break 
                    continue 
            else:
                mic.say(" I didn't understand the sign.")   
                continue 
        
def isValid(text):
    return any(word in text.upper() for word in WORDS)		

