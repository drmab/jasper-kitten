#Written by Jake Schultz
#TODO Add more lang support, limit number of results returned
import re
from urllib2 import Request, urlopen, URLError
import json
from client.mic import Mic

WORDS = ["HOROSCOPE", "WHAT", "IS", "MY","ASTROLOGY"]

def handle(text, mic, profile):
    # method to get the wiki summary
    phrases = ["YES", "NO", "ARIES", "TAURUS", "GEMINI", "CANCER","LEO", "VIRGO","LIBRA", "SCORPIO", "SAGITTARIUS", "CAPRICORN", "AQUARIUS", "PISCES"]

    # Comment out below two lines if you are using Jasper in TEXT Mode          
    astrol_stt_engine = mic.active_stt_engine.get_instance('astrol', phrases)
    mic = Mic(mic.speaker, mic.passive_stt_engine, astrol_stt_engine)

    while True:
        mic.say(" Which sign?")
        sign = mic.activeListen()
        #mic.say( "You said: "+sign)


        request = Request('https://api.moonbase.space/getHoroscope?sign='+sign+'&date=today')
        try:
            response = urlopen(request)
            data = json.load(response)
            # Parse the JSON to just get the extract. Always get the first summary.
            output = 'The horoscope for: '+data["text"]
            # final = output[output.keys()[0]]["extract"]
            mic.say(output)

        except URLError, e:
            mic.say("Unable to reach bus API.")

        mic.say(" Do you want another reading?")
        ans = mic.activeListen()
        if "NO" in ans.split():
            mic.say(" Alright then")
            break 
        continue 


def isValid(text):
    return bool(re.search(r'\bastrology\b',text, re.IGNORECASE))


