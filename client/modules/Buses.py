#Written by Jake Schultz
#TODO Add more lang support, limit number of results returned
import re
from urllib2 import Request, urlopen, URLError
import json

from client.mic import Mic

WORDS = ["BUS", "BUSES", "NEXT", "NEXT BUS"]

def handle(text, mic, profile):
    # Vocab used in Next  Buse Module
    phrases = ["NORTH", "SOUTH", "EAST", "WEST"]
    thebus = { "north": "94", "east": "185", "west": "24", "south": "185"}

    # Comment out below two lines if you are using Jasper in TEXT Mode          
    nextbus_stt_engine = mic.active_stt_engine.get_instance('nextbus', phrases)
    mic = Mic(mic.speaker, mic.passive_stt_engine, nextbus_stt_engine)

    mic.say(" Which direction?")
    direction = mic.activeListen()
    mic.say( "You said: "+direction)

    if direction == "south":
        getBusTime(185,"south")
        getBusTime(94,"south")
        getBusTime(85,"east")
    else:
        getBusTime(thebus[direction],direction)


def getBusTime(route,heading):
    # make a call to the API
    request = Request('https://api.moonbase.space/buses?route='+route+'&direction='+heading)
    mic.say("Let me see.")
    try:
        response = urlopen(request)
        mic.say('I got a response')
        data = json.load(response)
        # Parse the JSON to just get the extract. Always get the first summary.
        try:
          data["times"]
        except NameError:
          print  output = data
        else:
          output = 'The next '+route+' going '+heading+' are at: '+data["times"]

        mic.say(output)
    except URLError, e:
        mic.say("Unable to reach bus API.")


def isValid(text):
    bus= bool(re.search(r'\bBus\b',text, re.IGNORECASE))
    buses= bool(re.search(r'\bBuses\b',text, re.IGNORECASE))
    nextBus= bool(re.search(r'\bnext bus\b',text, re.IGNORECASE))

    if bus or buses or nextBus:
        return True
    else:
        return False

